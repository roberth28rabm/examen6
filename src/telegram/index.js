require('dotenv').config();
const Telegram = require('node-telegram-bot-api');
const bot = new Telegram(process.env.TOKEN_TELEGRAM_API, { polling: true });
//const fech = require ('node-fech');
const { Usuario, Vehiculo } = require('../models/index')
const fs = require('fs');


bot.onText(/^\/start/, function (msg) {
    console.log(msg);
    var chatId = msg.chat.id;
    var firstnameuser = msg.from.first_name;
    const usuario = {
        idusuario: msg.chat.first_name
    };
    Usuario.create([usuario]);

    bot.sendMessage(chatId, `Hola, ${firstnameuser} soy el bot de RentCars \n` +
        "¿En qué te puedo ayudar ? \n" +
        "Escribe el número de la opción a realizar \n" +
        "1. Reservar vehiculo 🚗 \n" +
        "2. Vehiculos en alquiler🔎 \n" +
        "3. Más información 🧐");
});

bot.on('message', function (msg) {

    var saludo = "hola";
    var firstnameuser = msg.from.first_name;
    var chatId = msg.chat.id;
    /*usuario = {
       idusuario: msg.chat.first_name
    };
    Usuario.create([usuario]);*/
    if (msg.text.toString().toLowerCase().includes(saludo)) {
        bot.sendMessage(chatId, `Hola, ${firstnameuser} soy el bot de RentCars \n` +
            "¿En qué te puedo ayudar ? \n" +
            "Escriba el número o la palabra de la opción a realizar \n" +
            "1. Reservar vehiculo 🚗 \n" +
            "2. Vehiculos en alquiler 🔎 \n" +
            "3. Más información 🧐");
    }

});

bot.on('message', function (msg) {

    var opcion1 = "opción";
    var opcion2 = "opcion";
    var opcion3 = "atras";
    var chatId = msg.chat.id;
    /*usuario = {
      idusuario: msg.chat.first_name
    };
    Usuario.create([usuario]);*/
    if (msg.text.toString().toLowerCase().includes(opcion1) || msg.text.toString().toLowerCase().includes(opcion2) || msg.text.toString().toLowerCase().includes(opcion3)) {
        bot.sendMessage(chatId,
            "Escriba el número o palabra de la opción a realizar \n" +
            "1. Reservar vehiculo 🚗 \n" +
            "2. Vehiculos en alquiler 🔎 \n" +
            "3. Más información 🧐");
    }

});

bot.on('message', function (msg) {

    var opcion = "1";
    var palabra = "reservar vehiculo";
    var chatId = msg.chat.id;

    if (msg.text.toString().toLowerCase().includes(opcion) || msg.text.toString().toLowerCase().includes(palabra)) {
        bot.sendMessage(chatId, `Escriba el número o la palabra de las opciones disponibles \n` +
            "- Solicitar vehículo🔎 \n" +
            "- Atrás🚗");
    }

});

bot.on('message', function (msg) {

    var palabra1 = "solicitar vehiculo";
    var palabra2 = "solicitar vehículo";
    var chatId = msg.chat.id;
    if (msg.text.toString().toLowerCase().includes(palabra1) || msg.text.toString().toLowerCase().includes(palabra2)) {
        bot.sendMessage(chatId, "Datos personales para el registro \n" +
            "Ingrese sus Apellidos y Nombres ");
        bot.on('message', function (msg) {
            //bot.sendMessage(msg.from.id, 'Escriba "Opción" para ir a las opciones principales');
            usuario = {
                idusuario: msg.chat.id,
                iduser: msg.chat.first_name,
                nomApe: msg.text,
                date: msg.date
            };
            Usuario.create([usuario]);
            //bot.sendMessage(msg.from.id, 'Su dato fue registrado correctamente, el personal se comunicara con usted'  );
        });
    }
});

bot.on('message', function (msg) {

    var opcion = "2";
    var palabra = "vehiculos en alquiler";
    var chatId = msg.chat.id;
    if (msg.text.toString().toLowerCase().includes(opcion) || msg.text.toString().toLowerCase().includes(palabra)) {
        bot.sendMessage(chatId, `Escriba la palabra de las opciones disponibles \n` +
            "-Familiar conchevino \n" +
            "-Familiar gris \n" +
            "-Furgoneta \n" +
            "-Limocina \n" +
            "-Chevrolet sail\n" +
            "-Bus Blanco\n" +
            "-Cuadron Blanco\n" +
            "-Ford Ranger\n" +
            "-Tesla Model S\n" +
            "-Montacargas\n" +
            "-Volqueta\n" +
            "-Tractor\n" +
            "-Toyota RAV4\n" + 
            "-Ferrari Roma\n" +
            "-Chevy Colorado\n"+
            "Escriba 'Opción' para ir a las opciones principales");
    }
});

bot.on('message', function (msg) {

    let img_vehiculo = msg.text.toLowerCase();
    const exists = fs.existsSync(`./src/public/${img_vehiculo}.jpg`);


    if (exists) {
        vehiculo = {
            nomVehiculo: msg.text
        };
        Vehiculo.create([vehiculo]);
        bot.sendPhoto(msg.from.id, `./src/public/${img_vehiculo}.jpg`);
        var red = fs.readFileSync(`./src/detalles/${img_vehiculo}.txt`).toString();
        bot.sendMessage(msg.from.id, red);
        bot.sendMessage(msg.from.id, `>Escriba "Opcion" para retornar al menú principal`);
    } else {
        //bot.sendMessage(msg.from.id, `El vehiculo ${img_vehiculo}.jpg no existe`);
    }

});
bot.on('message', function (msg) {

    var opcion = "3";
    var palabra = "Mas informacion";
    var palabra1 = "Más información";
    var chatId = msg.chat.id;
    if (msg.text.toString().toLowerCase().includes(opcion) || msg.text.toString().toLowerCase().includes(palabra) || msg.text.toString().toLowerCase().includes(palabra1)) {
        bot.sendMessage(chatId, `Escriba EDAD o ASPECTOS para mas imformación \n` +
            "-¿Que edad debo tener para alquilar un coche?\n" +
            "-¿Que aspectos son los más importantes a la hora de elgir un coche \n"+
            'Escriba "Opción" para ir a las opciones principales');
    }

});

bot.on('message', function (msg) {

    var palabra = "edad";
    var chatId = msg.chat.id;
    if (msg.text.toString().toLowerCase().includes(palabra)) {
        bot.sendMessage(chatId, `Edad \n` +
            "La mayoría de nuestros proveedores exigen que el conductor principal" +
            "tenga una edad de entre 21 y 70 años. Si el conductor es menor de 25 años o mayor de 70, es" +
            "posible que tenga que abonar una tasa adicional.\n"+
            'Escriba "Opción" para ir a las opciones principales');
    }

});

bot.on('message', function (msg) {

    var palabra = "aspectos";
    var chatId = msg.chat.id;
    if (msg.text.toString().toLowerCase().includes(palabra)) {
        bot.sendMessage(chatId, `Tamaño: viajará con más comodidad si escoge un vehículo que disponga de suficiente espacio para los pasajeros y las maletas.`);
        bot.sendMessage(chatId, `Política de combustible: ¿No piensas conducir mucho? Con la política de combustible Depósito al mismo nivel, puedes ahorrar mucho dinero.`);
        bot.sendMessage(chatId, `Ubicación: le resultará más cómodo escoger un proveedor cuya oficina de alquiler se encuentre dentro del mismo aeropuerto; sin embargo, los proveedores situados fuera del aeropuerto ofrecen un autobús de cortesía que le llevará a la oficina y suelen tener precios más económicos.`);
        bot.sendMessage(chatId, 'Escriba "Opción" para ir a las opciones principales');


    }
});



module.exports = { bot };