const mongoose = require('mongoose');

const {Schema}=mongoose

const vehiculoSchema = new Schema(
    {
        nomVehiculo:{type:String},        
        descripcion:{type:String},
        tipolicencia:{type:String}
    },
    {
        timestamps:true
    }
)

const Vehiculo = mongoose.model("Vehiculo", vehiculoSchema)
module.exports = Vehiculo;