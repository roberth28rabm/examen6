const express = require("express")
const router = express.Router();
const Vehiculo = require ("../models/vehiculo.model")

router.get("/mostrar", (req, res)=>{
    Vehiculo.find()
    .then((vehiculoss)=>{
        res.json(vehiculoss);
    })
    .catch(err=> console.log(err))

});

router.post("/agregar", (req, res)=>{
    const nomVehiculo = req.body.nomVehiculo;
    const descripcion = req.body.descripcion;
    const tipolicencia = req.body.tipolicencia;
    newvehiculo = new Vehiculo({
        nomVehiculo: nomVehiculo,
        descripcion: descripcion,
        tipolicencia: tipolicencia
        
    })
    newvehiculo.save()
    .then(vehiculos=>{
        res.json(vehiculos)
    })
    .catch(err=> console.log(err))
} )

router.put("/guardar/:id", (req, res) =>{
    let id= req.params.id;
    Vehiculo.findById(id)
        .then(vehiculo=>{
            vehiculo.nomVehiculo = req.body.nomVehiculo;
            vehiculo.descripcion = req.body.descripcion;
            vehiculo.tipolicencia = req.body.tipolicencia;
            vehiculo.save()
                .then(vehiculo=>{
                    res.send({
                        menssage:"Guardado satisfactoriamente",
                        status: "success",
                        vehiculo:vehiculo
                    })
                })
                .catch(err=> console.log(err))
        })
        .catch(err => console.log(err))
})

router.delete("/eliminar/:id", (req ,res)=>{
    let id = req.params.id;
    Vehiculo.findById(id)
        .then(vehiculo=>{
            vehiculo.delete()
                .then(vehiculo=>{
                    res.send({
                        menssage: "Eliminada satisfactoriamente",
                        status:"success",
                        vehiculo:vehiculo
                    })
                })
                .catch(err=> console.log(err))
        })
        .catch(err=> console.log(err))
})

module.exports = router