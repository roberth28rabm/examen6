//Librerias instaladas
const express = require('express');
const mongoose = require('mongoose');
const bodyParser = require('body-parser');
const cors = require("cors");
const path = require("path");
const app = express();
const vehiculoRoutes =require("./src/routes/vehiculos")
//Librerias locales
const { MONGO_URI } = require('./src/config/index');
const { Vehiculo } = require('/users/rober/documents/examen6/examen6/src/models');
//const BOT = require ('./src/telegram')

//Conectarnos a nuestras base de datos
mongoose.connect(MONGO_URI, { useNewUrlParser:true, useUnifiedTopology:true }).then (p=>{
    console.log('Conexion exitosa');
}).catch(err=>{
    console.log(err);
});
//formato
app.use(cors());
app.use(bodyParser.json());


//Puertoo
app.listen(5000);
app.get("/", (req, res)=>{
    res.send("Este es el servidor de testeo de Barre");
})

app.use("/index/vehiculoss", vehiculoRoutes);

//Telegram
require('./src/telegram');
module.exports=app;